from shutil import copyfile
from os.path import expanduser
home = expanduser("~")
print("------------------------------------------------------------")
print("Running Pre-Script")
f = open("src/buildnumber.h", "r")
version = f.read().split(" ")[2].split("\"")[1]
print("Got current version "+version)
build = version.split(".")[2]
build=str(int(build)+1)
newversion=version.split(".")[0]+"."+version.split(".")[1]+"."+build
print("Setting new version to "+newversion)
f = open("src/buildnumber.h", "w")
f.write("#define BUILD_NUMBER \""+newversion+"\"")
f.close() 
print("Done Pre-Script")
print("------------------------------------------------------------")
