Import("env", "projenv")
from shutil import copyfile

def before_upload(source, target, env):
    print("------------------------------------------------------------")
    print("Running before_upload")
    f = open("src/buildnumber.h", "r")
    version = f.read().split(" ")[2].split("\"")[1]
    f.close() 
    print("Saving hex version "+version)
    copyfile(".pio/build/teensy41/firmware.hex", "builds/"+version+".hex")
    print("Saving elf version "+version)
    copyfile(".pio/build/teensy41/firmware.elf", "builds/"+version+".elf")
    print ("Done before_upload")
    print("------------------------------------------------------------")


env.AddPreAction("upload", before_upload)
