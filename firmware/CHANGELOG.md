# io32artnet Changelog
### Build 1.0.1823
    - Release candidate
### Build 1.0.1745
    - Bugfix: Artnet Receiver rewritten; fixed memory allocation
### Build 1.0.1733
    - Bugfix: Artnet did not send all frames when multiple universes were configured on a single device
### Build 1.0.1706
    - DEBUG BUILD
    - Usage: Send full power to all connected leds [254,254,254] through Node-Red
    - Use your web browser to open the local web api (http://<our-device>)
    - The web api will show the devices config as well as the currently measured temperature
### Build 1.0.1694
    - Removed Web API
### Build 1.0.1691
    - Added Web API
    - Http request to the host will now return a json object with its configuration & statistics
### Build 1.0.1680
    - Minor Fixes
### Build 1.0.1594
    - New main display: now showing start universe & end universe
    - New menu system: click dial to enter menu, turn to select submenu
    - Fixed overheating bug
    - Config reset: press and hold test button
    - Reboot: press and hold dial button
    - New reboot screen, appearing before reboot
    - Dial turn optimized: now supporting slow turns & fast turns
    - Universe selector: start universe now goes up to 32256 (max supported universes = 512, max artnet universes = 32768)
### Build 1.0.1250
    - new button debouncer, allowing long press, double click etc
    - mac address tied to teensy uuid
    - mac address displayed on dhcp and ip config page
### Build 1.0.1229
    - 5 second press and release on test button resets the device
### Build 1.0.1187
    - randomize mac address
### Build 1.0.1182
    - stability: disregard Artnet data for other universes & leds not in range
### Build 1.0.1153
    - changed test button behavior: 1st click starts led test, 2nd click stops tests
    - during led test, no artnet messages are received / processed
    - ui now shows total amount of leds
    - max leds per strip raised to 6000
    - max universes raised to 512