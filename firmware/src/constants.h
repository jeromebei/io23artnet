#ifndef __CONSTANTS__
#define __CONSTANTS__
//=======================================================================================
// GLOBALS
//=======================================================================================
#define BOARD_VERSION 		    		1
#define COMPILE_DATE					__DATE__
#define COMPILE_TIME					__TIME__
#define IOPRODUCT_NAME					"io23artnet"
//=======================================================================================
// SERIAL
//=======================================================================================
#define BAUD_RATE    		    		57600
//=======================================================================================
// OLED
//=======================================================================================
#define OLED_SCREEN_WIDTH 				128 		// OLED display width, in pixels
#define OLED_SCREEN_HEIGHT 				64 			// OLED display height in pixels
#define OLED_RESET     					4 			// Reset pin # (or -1 if sharing Arduino reset pin)
#define OLED_SCREEN_ADDRESS 			0x3D 		// See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32
#define OLED_LINE_HEIGHT_1				11
#define OLED_LINE_HEIGHT_2				13
#define OLED_MAX_MENU_ITEMS 			5
//=======================================================================================
// NETWORK
//=======================================================================================
#define DEFAULT_IP_ADDRESS  			"10.0.0.20"
#define NETWORK_TIMEOUT                 10000
#define NETWORK_RESP_TIME               4000
//=======================================================================================
// ARTNET
//=======================================================================================
#define ARTNET_MAX_UNIVERSES 			512
#define ARTNET_MAX_START_UNIVERSE		32768-ARTNET_MAX_UNIVERSES
#define ARTNET_MAX_LEDS_PER_STRIP		8000
#define ARTNET_MAX_STRIPS			    8
#define ARTNET_MAX_LEDS			        ARTNET_MAX_STRIPS * ARTNET_MAX_LEDS_PER_STRIP
#define ARTNET_DISPLAYMEM 			    ARTNET_MAX_LEDS_PER_STRIP * 6
#define ARTNET_DRAWMEM 			        ARTNET_MAX_LEDS_PER_STRIP * 6
//=======================================================================================
// RGB LIGHT
//=======================================================================================
#define RGB_LIGHT_PIN_RED 			    25
#define RGB_LIGHT_PIN_GREEN 			28
#define RGB_LIGHT_PIN_BLUE			    29
#define RJ45_LIGHT_PIN			        15
//=======================================================================================
// UI
//=======================================================================================
#define UI_ENCODER_CLOCKWISE_PIN  		33
#define UI_ENCODER_ANTI_CLOCKWISE_PIN	34
#define UI_ENCODER_DIALBUTTON_PIN		35
#define UI_ENCODER_TESTBUTTON_PIN		36
#define WHEEL_DEBOUNCE_TIME             150
#define WHEEL_FAST_TURN_DIFF            4
#define LAST_ACTION_TIME                15000
#define LONG_TEST_TIME                  15000
//=======================================================================================
// TEMPERATURE
//=======================================================================================
#define TEMP_PIN                        41
#define TEMP_READ_INTERVAL              60000
#define TEMP_OVERHEAT                   98
//=======================================================================================
// GLOBAL ENUMS
//=======================================================================================

#endif
//=======================================================================================
