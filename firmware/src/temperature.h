#ifndef __TEMPERATURE__
#define __TEMPERATURE__
#include <Arduino.h>
//=======================================================================================
#include "constants.h"
#include "mbedmon.h"
#include "config.h"
#include "modules.h"
//=======================================================================================
namespace Temperature {
    //-----------------------------------------------------------------------------------
    float temp;
    unsigned long lastRead = 0;
    bool enabled=true;
    //-----------------------------------------------------------------------------------
    void  begin() {
        enabled=true;
    }
    //-----------------------------------------------------------------------------------
    void  loop() {
        if (!enabled) return;
        if (lastRead + TEMP_READ_INTERVAL > millis()) return;
        temp = ((float)analogRead(TEMP_PIN) * 5.0 / 1024.0) - 0.5;
        temp = temp / 0.01;
        lastRead = millis();
    }
    //-----------------------------------------------------------------------------------
    void stop(){
        enabled=false;
    }
    //-----------------------------------------------------------------------------------
    int read () {
        if (temp<0 || temp>TEMP_OVERHEAT+1) temp=0;
        //if (temp>TEMP_OVERHEAT) return Modules::stop();
        return static_cast<int>(round(temp));
    }
    //-----------------------------------------------------------------------------------
}
#endif
