#ifndef __Display_Dhcp__
#define __Display_Dhcp__
//=======================================================================================
#include <U8g2lib.h>

#include "constants.h"
#include "mbedmon.h"
#include "modules.h"
#include "display.base.h"
//=======================================================================================
namespace Display_Dhcp {
    //-----------------------------------------------------------------------------------
    void draw(U8G2_SSD1306_128X64_NONAME_F_HW_I2C&);
    void loop(U8G2_SSD1306_128X64_NONAME_F_HW_I2C&);
    void drawDhcp(U8G2_SSD1306_128X64_NONAME_F_HW_I2C&);
    bool initial_dhcp;
    //-----------------------------------------------------------------------------------
    void draw(U8G2_SSD1306_128X64_NONAME_F_HW_I2C &u8g2) {
        MBedMon::set("draw","dhcp");
        Display_Base::setLastAction();
        initial_dhcp = Config::store.dhcp;
        Display_Base::drawTitle("Enable Dhcp", u8g2);
        drawDhcp(u8g2);
        Display_Base::drawMac(u8g2);	        
    }
    //-----------------------------------------------------------------------------------
    void drawDhcp (U8G2_SSD1306_128X64_NONAME_F_HW_I2C &u8g2) {
        Display_Base::clearConfig(u8g2);
        u8g2.setFont(u8g2_font_6x10_tf);	
        u8g2.drawStr(5,28,(Config::store.dhcp ? "Yes":"No"));
        u8g2.sendBuffer();	        
    }
    //-----------------------------------------------------------------------------------
    void loop(U8G2_SSD1306_128X64_NONAME_F_HW_I2C &u8g2) {
        Display_Base::checkLastAction();
        if (Display_Base::wheel==WHEEL_NONE) return;
        Display_Base::setLastAction();
        Config::store.dhcp=(Config::store.dhcp ? false : true);
        drawDhcp(u8g2);
    }
    //-----------------------------------------------------------------------------------
    void btnEvent(Button btn, ButtonEvent evt){
        if (btn==BUTTON_DIAL && evt==BTN_CLICK) {
            if (initial_dhcp!=Config::store.dhcp) {
                Config::save();
                Display_Base::exitTo(DISPLAY_REBOOT);
            } else {
                Display_Base::exitTo(DISPLAY_MENU);
            }
        }
    }
    //-----------------------------------------------------------------------------------
}
#endif
