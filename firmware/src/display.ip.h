#ifndef __Display_Ip__
#define __Display_Ip__
//=======================================================================================
#include <U8g2lib.h>

#include "constants.h"
#include "config.h"
#include "display.base.h"
#include "mbedmon.h"
//=======================================================================================
namespace Display_Ip {
    //-----------------------------------------------------------------------------------
    void draw(U8G2_SSD1306_128X64_NONAME_F_HW_I2C&);
    void loop(U8G2_SSD1306_128X64_NONAME_F_HW_I2C&);
    void drawIp(U8G2_SSD1306_128X64_NONAME_F_HW_I2C&);
    uint8_t ip_portion=0, previous_ip_portion=0, arrowX=3;
    IPAddress initial_address;
    //-----------------------------------------------------------------------------------
    void draw(U8G2_SSD1306_128X64_NONAME_F_HW_I2C &u8g2) {
        MBedMon::set("draw","ip");
        ip_portion=0; previous_ip_portion=0;
        arrowX=3;
        initial_address=Config::store.ipAddress;
        Display_Base::setLastAction();
        Display_Base::drawTitle("IP Address", u8g2);
        drawIp(u8g2);        
        Display_Base::drawMac(u8g2);	        
    }
    //-----------------------------------------------------------------------------------
    void loop(U8G2_SSD1306_128X64_NONAME_F_HW_I2C &u8g2) {
        Display_Base::checkLastAction();
        if (Config::store.dhcp) return;
        if (Display_Base::wheel==WHEEL_LEFT) { //WHEEL TURNED LEFT
            Display_Base::setLastAction();
            if (Config::store.ipAddress[ip_portion]==0) Config::store.ipAddress[ip_portion]=254;
            else Config::store.ipAddress[ip_portion]--;
            return drawIp(u8g2);
        } else if (Display_Base::wheel==WHEEL_RIGHT) { //WHEEL TURNED RIGHT
            Display_Base::setLastAction();
            if (Config::store.ipAddress[ip_portion]==254) Config::store.ipAddress[ip_portion]=0;
            else Config::store.ipAddress[ip_portion]++;
            return drawIp(u8g2);
        } else if (Display_Base::wheel==WHEEL_FAST_LEFT) { //WHEEL TURNED LEFT
            Display_Base::setLastAction();
            if (Config::store.ipAddress[ip_portion]==10) Config::store.ipAddress[ip_portion]=254;
            else Config::store.ipAddress[ip_portion]-=10;
            return drawIp(u8g2);
        } else if (Display_Base::wheel==WHEEL_FAST_RIGHT) { //WHEEL TURNED RIGHT
            Display_Base::setLastAction();
            if (Config::store.ipAddress[ip_portion]==244) Config::store.ipAddress[ip_portion]=0;
            else Config::store.ipAddress[ip_portion]+=10;
            return drawIp(u8g2);
        }
        if (ip_portion!=previous_ip_portion) { //BUTTON PRESSED
            previous_ip_portion=ip_portion;
            Display_Base::setLastAction();
            return drawIp(u8g2);
        }
    }
    //-----------------------------------------------------------------------------------
    void drawIp(U8G2_SSD1306_128X64_NONAME_F_HW_I2C &u8g2) {
        Display_Base::clearConfig(u8g2);
        arrowX=3 + ip_portion*6*4;
        if (!Config::store.dhcp) {
            u8g2.setFont(u8g2_font_unifont_t_symbols);
            u8g2.drawGlyph(arrowX,25,0x25be);
            u8g2.drawGlyph(arrowX+6,25,0x25be);
            u8g2.drawGlyph(arrowX+12,25,0x25be);
            u8g2.drawGlyph(arrowX,42,0x25b4);
            u8g2.drawGlyph(arrowX+6,42,0x25b4);
            u8g2.drawGlyph(arrowX+12,42,0x25b4);
        }
        u8g2.setFont(u8g2_font_6x10_tf);	
        char ipaddress[26];
        sprintf(ipaddress,"%03d.%03d.%03d.%03d", Config::store.ipAddress[0], Config::store.ipAddress[1], Config::store.ipAddress[2], Config::store.ipAddress[3]);
        u8g2.drawStr(5,32,ipaddress);
        u8g2.sendBuffer();	        
    }
    //-----------------------------------------------------------------------------------
    void btnEvent(Button btn, ButtonEvent evt){
        if (btn==BUTTON_DIAL && evt==BTN_CLICK) {
            if (Config::store.dhcp) return Display_Base::exitTo(DISPLAY_MENU);
            ip_portion++;
            if (ip_portion==4) {
                if (initial_address!=Config::store.ipAddress) {
                    Config::save();
                    Display_Base::exitTo(DISPLAY_REBOOT);
                } else {
                    Display_Base::exitTo(DISPLAY_MENU);
                }
            } 
        }
    }
    //-----------------------------------------------------------------------------------
}
#endif
