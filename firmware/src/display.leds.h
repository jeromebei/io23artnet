#ifndef __Display_Leds__
#define __Display_Leds__
//=======================================================================================
#include <U8g2lib.h>

#include "constants.h"
#include "mbedmon.h"
#include "display.base.h"
//=======================================================================================
namespace Display_Leds {
    //-----------------------------------------------------------------------------------
    void draw(U8G2_SSD1306_128X64_NONAME_F_HW_I2C&);
    void loop(U8G2_SSD1306_128X64_NONAME_F_HW_I2C&);
    void drawLeds(U8G2_SSD1306_128X64_NONAME_F_HW_I2C&);
    unsigned int initial_leds;
    //-----------------------------------------------------------------------------------
    void draw(U8G2_SSD1306_128X64_NONAME_F_HW_I2C &u8g2) {
        MBedMon::set("draw","leds");
        Display_Base::setLastAction();
        initial_leds = Config::store.ledsPerStrip;
        //TODO: WHY THIS?
        Display_Base::drawTitle("", u8g2);
        Display_Base::drawTitle("Leds per Strip", u8g2);
        drawLeds(u8g2);
    }
    //-----------------------------------------------------------------------------------
    void drawLeds (U8G2_SSD1306_128X64_NONAME_F_HW_I2C &u8g2) {
        Display_Base::clearConfig(u8g2);
        u8g2.setFont(u8g2_font_6x10_tf);	
        char leds[5];
        sprintf(leds,"%04d", Config::store.ledsPerStrip);
        u8g2.drawStr(5,28,leds);
        u8g2.sendBuffer();	        
    }
    //-----------------------------------------------------------------------------------
    void loop(U8G2_SSD1306_128X64_NONAME_F_HW_I2C &u8g2) {
        Display_Base::checkLastAction();
        if (Display_Base::wheel==WHEEL_NONE) return;
        Display_Base::setLastAction();
        if (Display_Base::wheel==WHEEL_LEFT)  Config::store.ledsPerStrip = Config::store.ledsPerStrip == 0 ? ARTNET_MAX_LEDS_PER_STRIP : Config::store.ledsPerStrip-1;     
        if (Display_Base::wheel==WHEEL_RIGHT) Config::store.ledsPerStrip = Config::store.ledsPerStrip == ARTNET_MAX_LEDS_PER_STRIP ? 0 : Config::store.ledsPerStrip+1;     
        if (Display_Base::wheel==WHEEL_FAST_LEFT)  Config::store.ledsPerStrip = Config::store.ledsPerStrip <= 10 ? ARTNET_MAX_LEDS_PER_STRIP : Config::store.ledsPerStrip-10;     
        if (Display_Base::wheel==WHEEL_FAST_RIGHT) Config::store.ledsPerStrip = Config::store.ledsPerStrip >= ARTNET_MAX_LEDS_PER_STRIP-10 ? 0 : Config::store.ledsPerStrip+10;     

        drawLeds(u8g2);
    }
    //-----------------------------------------------------------------------------------
    void btnEvent(Button btn, ButtonEvent evt){
        if (btn==BUTTON_DIAL && evt==BTN_CLICK) {
            if (initial_leds!=Config::store.ledsPerStrip) {
                Config::save();
                Display_Base::restartModules();
            }
            Display_Base::exitTo(DISPLAY_MENU);
        }
    }
    //-----------------------------------------------------------------------------------
}
#endif
