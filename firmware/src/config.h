#ifndef __CONFIG__
#define __CONFIG__
#include <Arduino.h>
#include <EEPROM.h>
#include <TeensyID.h>
//=======================================================================================
#include "constants.h"
//=======================================================================================
#define CONFIG_START 		0
#define CONFIG_VERSION 	    24
typedef struct {
    unsigned int version;
    bool dhcp;
    bool debug;
    IPAddress ipAddress;
    byte mac[6];
    unsigned int ledStrips;
    unsigned int ledsPerStrip;
    unsigned int startUniverse;
    char uuid [24];
} ConfigStruct;
//=======================================================================================
namespace Config {
    //-----------------------------------------------------------------------------------
    ConfigStruct store;
    bool off=false;
    //-----------------------------------------------------------------------------------
    void save() {
        EEPROM.put(CONFIG_START, store);
    }
    //-----------------------------------------------------------------------------------
    void reset() {
        store.version=CONFIG_VERSION;
        sscanf(DEFAULT_IP_ADDRESS, "%hhu.%hhu.%hhu.%hhu", &store.ipAddress[0], &store.ipAddress[1], &store.ipAddress[2], &store.ipAddress[3]);
        store.startUniverse=0;
        store.ledStrips=8;
        store.ledsPerStrip=300;
        store.dhcp=true;
        store.debug=false;
	    uint8_t uid64[8];
	    teensyUID64(uid64);
	    sprintf(store.uuid, "%02x-%02x-%02x-%02x-%02x-%02x-%02x-%02x", uid64[0], uid64[1], uid64[2], uid64[3], uid64[4], uid64[5], uid64[6], uid64[7]);

        byte mac[6];
        teensyMAC(mac);
        memcpy (store.mac, mac, 6 * sizeof(byte));

        save();
    }
    //-----------------------------------------------------------------------------------
    void load() {
        EEPROM.get( CONFIG_START, store );
        if (store.version != CONFIG_VERSION) reset();
    }
    //-----------------------------------------------------------------------------------
    void begin() {
        load();
    }
    //-----------------------------------------------------------------------------------
    void powerDown() {
        off=true;
    }
    //-----------------------------------------------------------------------------------
    void powerUp() {
        off=false;
    }
    //-----------------------------------------------------------------------------------
}
#endif
