#ifndef __MBEDMON__
#define __MBEDMON__
//======================================================================================================
#include <Arduino.h>
#define CONSOLE_SERIAL SerialUSB
//======================================================================================================
namespace MBedMon {
	bool enabled=false;
	unsigned long lastTime;
	unsigned long __baud;
	//--------------------------------------------------------------------------------------------------
	void baud(unsigned long baud)   	{__baud=baud;}
	void startDebug()   				{enabled=true;}
	void stopDebug()    				{enabled=false;}
	void sendPre(const char* key) {
		if (!enabled) return;
		CONSOLE_SERIAL.print("[\"");
		CONSOLE_SERIAL.print(key);
		CONSOLE_SERIAL.print("\",\"");
		delay(10);			
	};
	//--------------------------------------------------------------------------------------------------
	void sendPost() {
		if (!enabled) return;
		CONSOLE_SERIAL.println("\"]");
		CONSOLE_SERIAL.flush();
		delay(10);			
	};		
	//--------------------------------------------------------------------------------------------------
	void set(const char* key, const char* val) 			{if (!enabled) return; sendPre(key); CONSOLE_SERIAL.print(val); sendPost();}
	void set(const char* key, const int val) 			{if (!enabled) return; sendPre(key); CONSOLE_SERIAL.print(val); sendPost();}
	void set(const char* key, const long val) 			{if (!enabled) return; sendPre(key); CONSOLE_SERIAL.print(val); sendPost();}
	void set(const char* key, const unsigned long val) 	{if (!enabled) return; sendPre(key); CONSOLE_SERIAL.print(val); sendPost();}
	void set(const char* key, const double val)			{if (!enabled) return; sendPre(key); CONSOLE_SERIAL.print(val); sendPost();}
	void set(const char* key, const float val)			{if (!enabled) return; sendPre(key); CONSOLE_SERIAL.print(val); sendPost();}
	//--------------------------------------------------------------------------------------------------
	void begin () {
		enabled=true;
		lastTime=0;
		CONSOLE_SERIAL.begin(__baud);
		//while (!CONSOLE_SERIAL) {}
		CONSOLE_SERIAL.println();			
	};
	//--------------------------------------------------------------------------------------------------
	void stop(){
		enabled=false;
	}
	//--------------------------------------------------------------------------------------------------
	void loop() {
		
	};		
	//--------------------------------------------------------------------------------------------------
};
#endif