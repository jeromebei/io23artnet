#ifndef __MODULES__
#define __MODULES__
//=======================================================================================
//=======================================================================================
typedef void (*ModuleFunc) (void);
typedef struct {ModuleFunc begin; ModuleFunc loop;  ModuleFunc stop;} ModuleStruct;
const unsigned int __max_modules = 20;
//=======================================================================================
#include "constants.h"
//=======================================================================================
namespace Modules {
        //-------------------------------------------------------------------------------
        unsigned int __module_count = 0;
        ModuleStruct __modules[__max_modules];
        //--------------------------------------------------------------------------------
        void begin() {
                for (unsigned int i = 0; i < __module_count; i++) __modules[i].begin();
        }
        //--------------------------------------------------------------------------------
        void loop() {
                for (unsigned int i = 0; i < __module_count; i++) __modules[i].loop();
        }
        //--------------------------------------------------------------------------------
        void stop() {
                for (unsigned int i = 0; i < __module_count; i++) __modules[i].stop();
        }
        //--------------------------------------------------------------------------------
        void add (ModuleFunc begin, ModuleFunc loop, ModuleFunc stop) {
                __module_count++;
                if (__module_count > __max_modules) return;
                __modules[__module_count-1]=ModuleStruct {begin, loop, stop};
        }    
        //--------------------------------------------------------------------------------
        void reboot() {
                SCB_AIRCR = 0x05FA0004;             
        }
        //--------------------------------------------------------------------------------
}
#endif

