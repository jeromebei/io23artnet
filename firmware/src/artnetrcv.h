#ifndef __ARTNETRECEIVER__
#define __ARTNETRECEIVER__
#include <SPI.h>
#include <OctoWS2811.h>
#include <Artnet.h>
#include <NativeEthernet.h>
//=======================================================================================
#include "constants.h"
#include "config.h"
#include "mbedmon.h"
#include "display.h"
#include "display.base.h"
//=======================================================================================
namespace ArtnetRcv {
    int numLeds, numberOfChannels, startUniverse, universeCount, previousDataLength;
    Artnet artnet;
    bool enabled=true;
    bool sendFrame=true;
    DMAMEM int displayMemory[ARTNET_DISPLAYMEM];
    int drawingMemory[ARTNET_DRAWMEM];
    bool universesReceived[ARTNET_MAX_UNIVERSES];
    const int cnf = WS2811_GRB | WS2811_800kHz;
    uint8_t pinList[8] = {2,3,4,5,6,7,8,9};

    unsigned long __testTime=0;
    int lt_r=127, lt_g=0, lt_b=0, rdir=-10, gdir=10, bdir=10;
    int lt_black;
    bool __testing=false;

    //OctoWS2811 leds(ARTNET_MAX_LEDS_PER_STRIP, displayMemory, NULL, cnf, 8, pinList);
    OctoWS2811 leds(ARTNET_MAX_LEDS_PER_STRIP, displayMemory, drawingMemory, cnf, 8, pinList);
    //-----------------------------------------------------------------------------------
    void callback (uint16_t universe, uint16_t length, uint8_t sequence, uint8_t* data) {
        if (universe < Config::store.startUniverse) return;
        if (universe > Config::store.startUniverse+universeCount) return;
        sendFrame = true;

        if ((universe - startUniverse) < universeCount) universesReceived[universe - startUniverse] = 1;

        for (int i = 0 ; i < universeCount ; i++) {
            if (universesReceived[i] == 0) {
                sendFrame = 0;
                break;
            }
        }
        
        for (int i = 0; i < length / 3; i++) {
            int led = i + (universe - startUniverse) * (previousDataLength / 3);
            if (led < numLeds) leds.setPixel(led, data[i * 3], data[i * 3 + 1], data[i * 3 + 2]);
            else MBedMon::set("exceed",led);
        }
        previousDataLength = length;

        if (sendFrame) {
            leds.show();
            memset(universesReceived, 0, universeCount);
        } 

        //leds.show();
    }
    //-----------------------------------------------------------------------------------
    void test() {
        MBedMon::set("artnet-test",static_cast<int>(numLeds));
        for (int l = 0 ; l < numLeds ; l++) leds.setPixel(l, 63, 0, 0);  leds.show(); delay(50);
        for (int l = 0 ; l < numLeds ; l++) leds.setPixel(l, 63, 63, 0); leds.show(); delay(50);
        for (int l = 0 ; l < numLeds ; l++) leds.setPixel(l, 0, 63, 63); leds.show(); delay(50);
        for (int l = 0 ; l < numLeds ; l++) leds.setPixel(l, 63, 63, 0); leds.show(); delay(50);
        for (int l = 0 ; l < numLeds ; l++) leds.setPixel(l, 63, 0, 0);  leds.show(); delay(50);
        for (int l = 0 ; l < numLeds ; l++) leds.setPixel(l, 0, 0, 0);   leds.show();
    }
    //-----------------------------------------------------------------------------------
    void longTest(bool testing) {
        __testing=testing;
        if (__testing) __testTime=millis();
        else for (int i = 0 ; i < numLeds  ; i++) leds.setPixel(i, 0, 0, 0);leds.show();
    }
    //-----------------------------------------------------------------------------------
    void __longTest() {
        if (__testTime>millis()-20) return;
        if (lt_r<=0) {rdir=1;} else if (lt_r>=127) {rdir=-2;} lt_r+=rdir;
        if (lt_g<=0) {gdir=3;} else if (lt_g>=127) {gdir=-4;} lt_g+=gdir;
        if (lt_b<=0) {bdir=5;} else if (lt_b>=127) {bdir=-6;} lt_b+=bdir;

        if (lt_r<0) lt_r=0; if (lt_g<0) lt_g=0; if (lt_b<0) lt_b=0;
        if (lt_r>127) lt_r=127; if (lt_g>127) lt_g=127; if (lt_b>127) lt_b=127;

        for (int l = 0 ; l < numLeds ; l++) leds.setPixel(l, lt_r, lt_g, lt_b);
        lt_black++;
        if (lt_black+4 > numLeds) lt_black=0;
        leds.setPixel(lt_black, 0, 0, 0);
        leds.setPixel(lt_black+1, 50, 50, 50);
        leds.setPixel(lt_black+2, 90, 90, 90);
        leds.setPixel(lt_black+3, 50, 50, 50);
        leds.setPixel(lt_black+4, 0, 0, 0);
        leds.show();
        __testTime=millis();
    }
    //-----------------------------------------------------------------------------------
    void begin() {
        enabled=true;
        sendFrame = true;
        Display::status("artnet start");
        numLeds = Config::store.ledsPerStrip * Config::store.ledStrips;

        if (numLeds > ARTNET_MAX_LEDS) numLeds = ARTNET_MAX_LEDS;
        numberOfChannels = numLeds * 3;

        startUniverse = Config::store.startUniverse;
        universeCount = numberOfChannels / 512 + ((numberOfChannels % 512) ? 1 : 0);
        Display_Base::universeCount=universeCount;
        previousDataLength = 0;

        byte ip[] = {Config::store.ipAddress[0], Config::store.ipAddress[1], Config::store.ipAddress[2], Config::store.ipAddress[3]};
        Display::status("artnet begin");
        artnet.begin(Config::store.mac, ip);
        Display::status("leds begin");
        //leds.begin(Config::store.ledsPerStrip, displayMemory, NULL, cnf, Config::store.ledStrips, pinList);
        leds.begin(Config::store.ledsPerStrip, displayMemory, drawingMemory, cnf, Config::store.ledStrips, pinList);
        test();
        artnet.setArtDmxCallback(callback);
        Display::status("artnet ready");
    }
    //-----------------------------------------------------------------------------------
    void stop(){
        for (int i = 0 ; i < numLeds  ; i++) leds.setPixel(i, 0, 0, 0);
        leds.show();        
        enabled=false;
    }
    //-----------------------------------------------------------------------------------
    void restart(){
        begin();
    }
    //-----------------------------------------------------------------------------------
    void  loop() {
        if (!enabled) return;
        if (__testing) __longTest();
        else artnet.read();
    }
    //-----------------------------------------------------------------------------------
}
#endif
