#ifndef __Display_Menu__
#define __Display_Menu__
//=======================================================================================
#include <U8g2lib.h>

#include "constants.h"
#include "mbedmon.h"
#include "config.h"
#include "display.base.h"
//=======================================================================================
namespace Display_Menu {
    //-----------------------------------------------------------------------------------
    void draw(U8G2_SSD1306_128X64_NONAME_F_HW_I2C&);
    void loop(U8G2_SSD1306_128X64_NONAME_F_HW_I2C&);
    //-----------------------------------------------------------------------------------
    const uint8_t menu_count=6, max_menu_display=5;
    uint8_t selected_menu=0;
    const char *menu_items [menu_count]{"< Back","Dhcp","Ip Address","Universe","Led Strips","Leds per Strip"};
    //-----------------------------------------------------------------------------------
    void draw(U8G2_SSD1306_128X64_NONAME_F_HW_I2C &u8g2) {
        MBedMon::set("draw","menu");
        Display_Base::setLastAction();
        u8g2.clearDisplay();
        u8g2.setDrawColor(1);
        u8g2.setFont(u8g2_font_6x10_tf);	

        uint8_t first_menu=(selected_menu-(menu_count-max_menu_display)<0 ? 0 : selected_menu-(menu_count-max_menu_display));
        uint8_t last_menu=(first_menu+max_menu_display > menu_count ? menu_count : first_menu+max_menu_display);

        uint8_t y=0, box_height=12,text_height=10, text_space=2;
        for (uint8_t m=first_menu; m<last_menu; m++) {
            uint8_t yBox  = y*box_height;
            uint8_t yText = yBox+text_height-(y==0?0:text_space);

            if (m==selected_menu) {
                u8g2.drawBox(0,yBox,128,box_height);
                u8g2.setDrawColor(0);
            } else {
                u8g2.setDrawColor(1);
            }
            u8g2.drawStr(3,yText,menu_items[m]);	
            y++;
        }
        u8g2.sendBuffer();
    }
    //-----------------------------------------------------------------------------------
    void loop(U8G2_SSD1306_128X64_NONAME_F_HW_I2C &u8g2) {
        Display_Base::checkLastAction();
        if (Display_Base::wheel==WHEEL_NONE) return;
        Display_Base::setLastAction();
        if (Display_Base::wheel==WHEEL_LEFT || Display_Base::wheel==WHEEL_FAST_LEFT) selected_menu > 0 ? selected_menu-- : 0;
        else if (Display_Base::wheel==WHEEL_RIGHT || Display_Base::wheel==WHEEL_FAST_RIGHT) (selected_menu>=menu_count-1) ? menu_count-1 : selected_menu++;
        draw(u8g2);
    }
    //-----------------------------------------------------------------------------------
    void btnEvent(Button btn, ButtonEvent evt){
        if (btn==BUTTON_DIAL && evt==BTN_CLICK) {
            Mode mode = DISPLAY_MAIN;
            MBedMon::set("menu",selected_menu);
            switch(selected_menu) {
                case 0: mode = DISPLAY_MAIN; break;
                case 1: mode = DISPLAY_CONFIG_DHCP; break;
                case 2: mode = DISPLAY_CONFIG_IP; break;
                case 3: mode = DISPLAY_CONFIG_UNIVERSE; break;
                case 4: mode = DISPLAY_CONFIG_STRIPS; break;
                case 5: mode = DISPLAY_CONFIG_LEDS_PER_STRIP; break;
            }
            return Display_Base::exitTo(mode);
        }
    }
    //-----------------------------------------------------------------------------------
}
#endif
