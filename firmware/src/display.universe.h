#ifndef __Display_Universe__
#define __Display_Universe__
//=======================================================================================
#include <U8g2lib.h>

#include "constants.h"
#include "mbedmon.h"
#include "display.base.h"
//=======================================================================================
namespace Display_Universe {
    //-----------------------------------------------------------------------------------
    void draw(U8G2_SSD1306_128X64_NONAME_F_HW_I2C&);
    void loop(U8G2_SSD1306_128X64_NONAME_F_HW_I2C&);
    void drawUniverse(U8G2_SSD1306_128X64_NONAME_F_HW_I2C&);
    unsigned int initial_universe;
    //-----------------------------------------------------------------------------------
    void draw(U8G2_SSD1306_128X64_NONAME_F_HW_I2C &u8g2) {
        MBedMon::set("draw","universe");
        Display_Base::setLastAction();
        initial_universe = Config::store.startUniverse;
        Display_Base::drawTitle("Start Universe", u8g2);
        drawUniverse(u8g2);
    }
    //-----------------------------------------------------------------------------------
    void drawUniverse (U8G2_SSD1306_128X64_NONAME_F_HW_I2C &u8g2) {
        Display_Base::clearConfig(u8g2);
        u8g2.setFont(u8g2_font_6x10_tf);	
        char universe[5];
        sprintf(universe,"%05d", Config::store.startUniverse);
        u8g2.drawStr(5,28,universe);
        u8g2.sendBuffer();	        
    }
    //-----------------------------------------------------------------------------------
    void loop(U8G2_SSD1306_128X64_NONAME_F_HW_I2C &u8g2) {
        Display_Base::checkLastAction();
        if (Display_Base::wheel==WHEEL_NONE) return;
        Display_Base::setLastAction();
        if (Display_Base::wheel==WHEEL_LEFT)  Config::store.startUniverse = Config::store.startUniverse == 0 ? ARTNET_MAX_START_UNIVERSE : Config::store.startUniverse-1;     
        if (Display_Base::wheel==WHEEL_RIGHT) Config::store.startUniverse = Config::store.startUniverse == ARTNET_MAX_START_UNIVERSE ? 0 : Config::store.startUniverse+1;     
        if (Display_Base::wheel==WHEEL_FAST_LEFT)  Config::store.startUniverse = Config::store.startUniverse <= 10 ? ARTNET_MAX_START_UNIVERSE : Config::store.startUniverse-10;     
        if (Display_Base::wheel==WHEEL_FAST_RIGHT) Config::store.startUniverse = Config::store.startUniverse >= ARTNET_MAX_START_UNIVERSE-10 ? 0 : Config::store.startUniverse+10;     
        drawUniverse(u8g2);
    }
    //-----------------------------------------------------------------------------------
    void btnEvent(Button btn, ButtonEvent evt){
        if (btn==BUTTON_DIAL && evt==BTN_CLICK) {
            if (initial_universe!=Config::store.startUniverse) {
                Config::save();
                Display_Base::restartModules();
            }
            Display_Base::exitTo(DISPLAY_MENU);
        }
    }
    //-----------------------------------------------------------------------------------
}
#endif
