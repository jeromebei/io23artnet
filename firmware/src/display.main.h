#ifndef __Display_Main__
#define __Display_Main__
//=======================================================================================
#include <U8g2lib.h>

#include "constants.h"
#include "buildnumber.h"
#include "config.h"
#include "mbedmon.h"
#include "temperature.h"
#include "display.base.h"
//=======================================================================================
namespace Display_Main {
    char ipaddress[26];
    unsigned long nextping = 0;
    uint16_t glyph = 0x25CB;
    char* __status = new char[126];
    int universeCount = 0;
    //-----------------------------------------------------------------------------------
    void draw(U8G2_SSD1306_128X64_NONAME_F_HW_I2C&);
    void status(U8G2_SSD1306_128X64_NONAME_F_HW_I2C&);
    void ping(U8G2_SSD1306_128X64_NONAME_F_HW_I2C&);
    void ip(U8G2_SSD1306_128X64_NONAME_F_HW_I2C&);
    void loop(U8G2_SSD1306_128X64_NONAME_F_HW_I2C&);
    //-----------------------------------------------------------------------------------
    void draw(U8G2_SSD1306_128X64_NONAME_F_HW_I2C &u8g2) {
        MBedMon::set("draw","main");
        u8g2.clearDisplay();
        u8g2.setDrawColor(1);
        u8g2.drawLine(0,0,127,0);
        u8g2.drawLine(0,12,127,12);
        u8g2.drawLine(0,25,127,25);
        u8g2.drawLine(0,38,127,38);

        u8g2.drawLine(0,51,127,51);
        u8g2.drawLine(70,0,70,11);
        u8g2.drawLine(45,26,45,50);
        u8g2.drawLine(96,26,96,50);

        u8g2.setFont(u8g2_font_unifont_t_symbols);
        u8g2.drawGlyph(0, 23, 0x260E);   // IP Address
        u8g2.drawGlyph(0, 38, 0x21A6);   // Start Universe
        u8g2.drawGlyph(50, 38, 0x21A4);  // End Universe
        u8g2.drawGlyph(0, 50, 0x25CE);   // Leds per Strip
        u8g2.drawGlyph(50, 50, 0x25CD);  // Leds total
        u8g2.drawGlyph(100, 50, 0x260A); // Strips
        u8g2.drawGlyph(113,37, 0x00B0);  // °C

        u8g2.setFont(u8g2_font_6x10_tf);	
        u8g2.drawStr(0,10,IOPRODUCT_NAME);	
        u8g2.drawStr(76,10,BUILD_NUMBER);	

        char __cnf[128];
        sprintf(__cnf, "%05d", Config::store.startUniverse);
        u8g2.drawStr(12,36,__cnf);	

        sprintf(__cnf, "%04d", Config::store.ledsPerStrip);
        u8g2.drawStr(12,49,__cnf);

        sprintf(__cnf, "%d", Config::store.ledStrips);
        u8g2.drawStr(112,49,__cnf);

        sprintf(__cnf, "%05d", 0);
        u8g2.setFont(u8g2_font_6x10_tf);	
        u8g2.setDrawColor(1);
        u8g2.drawStr(62,36,__cnf);

        sprintf(__cnf, "%05d", Config::store.ledsPerStrip * Config::store.ledStrips);
        u8g2.drawStr(62,49,__cnf);	
        u8g2.sendBuffer();

        ip(u8g2);
        status(u8g2);
        loop(u8g2);
    }
    //-----------------------------------------------------------------------------------
    void ip(U8G2_SSD1306_128X64_NONAME_F_HW_I2C &u8g2) {
        u8g2.setDrawColor(0);
        u8g2.drawBox(12,13,128,12);
        u8g2.setDrawColor(1);
        u8g2.setFont(u8g2_font_6x10_tf);	
        if (Config::store.ipAddress[0]==0 && Config::store.ipAddress[1]==0 && Config::store.ipAddress[2]==0 && Config::store.ipAddress[3]==0) {
            u8g2.drawStr(12,23,"ip not set");        
        } else {
            sprintf(ipaddress, "%d.%d.%d.%d", Config::store.ipAddress[0], Config::store.ipAddress[1], Config::store.ipAddress[2], Config::store.ipAddress[3]);
            u8g2.drawStr(12,23,ipaddress);
        }
        u8g2.sendBuffer();	
    }    
    //-----------------------------------------------------------------------------------
    void status(U8G2_SSD1306_128X64_NONAME_F_HW_I2C &u8g2) {
        u8g2.setFont(u8g2_font_6x10_tf);	
        u8g2.setDrawColor(0);
        u8g2.drawBox(12,52,128,12);
        u8g2.setDrawColor(1);
        u8g2.drawStr(12,62,__status);
        u8g2.sendBuffer();	
    }
    //-----------------------------------------------------------------------------------
    void loop(U8G2_SSD1306_128X64_NONAME_F_HW_I2C &u8g2) {
        if (nextping > millis()) return;
        if (glyph==0x25CB) glyph = 0x25CF; else glyph = 0x25CB;
        u8g2.setFont(u8g2_font_unifont_t_symbols);
        u8g2.setDrawColor(1);
        u8g2.drawGlyph(0, 64, glyph);        
        u8g2.setFont(u8g2_font_6x10_tf);	
        char __temp[2];
        sprintf(__temp, "%02d C", Temperature::read());
        u8g2.drawStr(102,36,__temp);	
        
        if (Display_Base::universeCount != universeCount) {
            universeCount = Display_Base::universeCount;
            char __cnf[128];
            sprintf(__cnf, "%05d", Config::store.startUniverse+universeCount-1);
            u8g2.setFont(u8g2_font_6x10_tf);	
            u8g2.setDrawColor(1);
            u8g2.drawStr(62,36,__cnf);	
        }

        nextping = millis() + 500; 
        u8g2.sendBuffer();	
    }
    //-----------------------------------------------------------------------------------
    void btnEvent(Button btn, ButtonEvent evt){
        if (btn==BUTTON_DIAL && evt==BTN_CLICK) return Display_Base::exitTo(DISPLAY_MENU);
        if (btn==BUTTON_TEST && evt==BTN_CLICK) return Display_Base::switchTest();
        if (btn==BUTTON_DIAL && evt==BTN_LONGPRESS_DURING) {return Display_Base::exitTo(DISPLAY_REBOOT);}
        if (btn==BUTTON_TEST && evt==BTN_LONGPRESS_DURING) {Config::reset();return Display_Base::exitTo(DISPLAY_REBOOT);}
    }
    //-----------------------------------------------------------------------------------
}
#endif
