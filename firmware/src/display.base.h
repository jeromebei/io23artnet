#ifndef __Display_Base__
#define __Display_Base__
//=======================================================================================
#include <U8g2lib.h>
#include <OneButton.h>

#include "constants.h"
#include "mbedmon.h"
//=======================================================================================
enum Mode {DISPLAY_OFF, DISPLAY_MAIN, DISPLAY_MENU, DISPLAY_CONFIG_DHCP, DISPLAY_CONFIG_IP, DISPLAY_CONFIG_UNIVERSE, DISPLAY_CONFIG_STRIPS, DISPLAY_CONFIG_LEDS_PER_STRIP, DISPLAY_REBOOT};
enum Wheel {WHEEL_NONE=0,WHEEL_LEFT=-1,WHEEL_RIGHT=1, WHEEL_FAST_LEFT=-2, WHEEL_FAST_RIGHT=2};
enum Button {BUTTON_DIAL,BUTTON_TEST};
enum ButtonEvent {BTN_CLICK,BTN_DOUBLE_CLICK,BTN_LONGPRESS_START,BTN_LONGPRESS_STOP,BTN_LONGPRESS_DURING};    
namespace Display_Base {
    //-----------------------------------------------------------------------------------
    OneButton btnDial (UI_ENCODER_DIALBUTTON_PIN, true);
    OneButton btnTest (UI_ENCODER_TESTBUTTON_PIN, true);
    //-----------------------------------------------------------------------------------
    Wheel wheel = WHEEL_NONE;
    unsigned long last_action=0;
    //-----------------------------------------------------------------------------------
    int universeCount=0;
    //-----------------------------------------------------------------------------------
    typedef void (*RestartFunc) (void);
    typedef struct {RestartFunc restart;} RestartModuleStruct;
    RestartModuleStruct __restartModules[10];
    unsigned int __restart_callback_count = 0;
	void restartCallback (RestartFunc func) {
        __restart_callback_count++;
        __restartModules[__restart_callback_count-1]=RestartModuleStruct {func};
	}        
    void restartModules() {
        for (unsigned int i = 0; i < __restart_callback_count; i++) __restartModules[i].restart();
    }
    //-----------------------------------------------------------------------------------
    typedef void (*TestFunc) (bool);
    typedef struct {TestFunc test;} TestFunctionStruct;
    TestFunctionStruct __testFunctions[10];
    unsigned int __test_callback_count = 0;
    bool __testing = false;
	void testCallback (TestFunc func) {
        __test_callback_count++;
        __testFunctions[__test_callback_count-1]=TestFunctionStruct {func};
	}        
    void switchTest(){
        __testing = !__testing;
        for (unsigned int i = 0; i < __test_callback_count; i++) __testFunctions[i].test(__testing); 
    }
    //-----------------------------------------------------------------------------------
    typedef void (*ExitFunc) (Mode);
    ExitFunc __exitFunc;
    void exitTo(Mode mode) {return __exitFunc(mode);}
    void exitCallback (ExitFunc func) { __exitFunc=func;}
    //-----------------------------------------------------------------------------------
    void setLastAction(){last_action=millis();}
    void checkLastAction(){if (millis()-last_action > LAST_ACTION_TIME) return exitTo(DISPLAY_MAIN);}
    //-----------------------------------------------------------------------------------
    void drawTitle(const char title[128], U8G2_SSD1306_128X64_NONAME_F_HW_I2C &u8g2) {
        u8g2.clearBuffer();
        u8g2.clearDisplay();
        u8g2.setFont(u8g2_font_6x10_tf);	
        u8g2.drawStr(0,10,title);
        u8g2.setDrawColor(1);
        u8g2.drawLine(0,0,127,0);
        u8g2.drawLine(0,12,127,12);
        u8g2.sendBuffer();
    }
    //-----------------------------------------------------------------------------------
    void drawMac(U8G2_SSD1306_128X64_NONAME_F_HW_I2C &u8g2){
        char __mac[64];
        sprintf(__mac, "%02X:%02X:%02X:%02X:%02X:%02X", Config::store.mac[0],Config::store.mac[1],Config::store.mac[2],Config::store.mac[3],Config::store.mac[4],Config::store.mac[5]);
        u8g2.setFont(u8g2_font_6x10_tf);	
        u8g2.drawLine(0,53,127,53);
        u8g2.drawStr(12,64,__mac);	
        u8g2.setFont(u8g2_font_unifont_t_symbols);
        u8g2.drawGlyph(0, 65, 0x260F);   
        u8g2.sendBuffer();	        
    }
    //-----------------------------------------------------------------------------------
    void clearConfig(U8G2_SSD1306_128X64_NONAME_F_HW_I2C &u8g2){
        u8g2.setDrawColor(0);
        u8g2.drawBox(0,13,128,39);
        u8g2.setDrawColor(1);
        u8g2.sendBuffer();	        
    //-----------------------------------------------------------------------------------
    }
}
#endif
