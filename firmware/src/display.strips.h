#ifndef __Display_Strips__
#define __Display_Strips__
//=======================================================================================
#include <U8g2lib.h>

#include "constants.h"
#include "mbedmon.h"
#include "display.base.h"
//=======================================================================================
namespace Display_Strips {
    //-----------------------------------------------------------------------------------
    void draw(U8G2_SSD1306_128X64_NONAME_F_HW_I2C&);
    void loop(U8G2_SSD1306_128X64_NONAME_F_HW_I2C&);
    void drawStrips(U8G2_SSD1306_128X64_NONAME_F_HW_I2C&);
    unsigned int initial_strips;
    //-----------------------------------------------------------------------------------
    void draw(U8G2_SSD1306_128X64_NONAME_F_HW_I2C &u8g2) {
        MBedMon::set("draw","strips");
        Display_Base::setLastAction();
        initial_strips = Config::store.ledStrips;
        Display_Base::drawTitle("Led Strips", u8g2);
        drawStrips(u8g2);
    }
    //-----------------------------------------------------------------------------------
    void drawStrips (U8G2_SSD1306_128X64_NONAME_F_HW_I2C &u8g2) {
        Display_Base::clearConfig(u8g2);
        u8g2.setFont(u8g2_font_6x10_tf);	
        char strips[1];
        sprintf(strips,"%01d", Config::store.ledStrips);
        u8g2.drawStr(5,28,strips);
        u8g2.sendBuffer();	        
    }
    //-----------------------------------------------------------------------------------
    void loop(U8G2_SSD1306_128X64_NONAME_F_HW_I2C &u8g2) {
        Display_Base::checkLastAction();
        if (Display_Base::wheel==WHEEL_NONE) return;
        Display_Base::setLastAction();
        if (Display_Base::wheel==WHEEL_LEFT)  Config::store.ledStrips = Config::store.ledStrips == 0 ? ARTNET_MAX_STRIPS : Config::store.ledStrips-1;     
        if (Display_Base::wheel==WHEEL_RIGHT) Config::store.ledStrips = Config::store.ledStrips == ARTNET_MAX_STRIPS ? 0 : Config::store.ledStrips+1;     
        drawStrips(u8g2);
    }
    //-----------------------------------------------------------------------------------
    void btnEvent(Button btn, ButtonEvent evt){
        if (btn==BUTTON_DIAL && evt==BTN_CLICK) {
            if (initial_strips!=Config::store.ledStrips) {
                Config::save();
                Display_Base::restartModules();
            }
            Display_Base::exitTo(DISPLAY_MENU);
        }
    }
    //-----------------------------------------------------------------------------------
}
#endif
