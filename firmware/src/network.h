#ifndef __NETWORK__
#define __NETWORK__
#include <Arduino.h>
#include <NativeEthernet.h>
//=======================================================================================
#include "constants.h"
#include "mbedmon.h"
#include "config.h"
#include "display.h"
//=======================================================================================
namespace Network {
    
    //-----------------------------------------------------------------------------------
    void  begin() {
        Display::status("network init");
        if (Config::store.dhcp) {
            bool success = (Ethernet.begin(Config::store.mac, NETWORK_TIMEOUT, NETWORK_RESP_TIME) == 1);
            if (success) Config::store.ipAddress=Ethernet.localIP();
            else Config::store.ipAddress=IPAddress(0,0,0,0);
            Config::save();
        } else {
            if (Config::store.ipAddress[0]==0 && Config::store.ipAddress[1]==0 && Config::store.ipAddress[2]==0 && Config::store.ipAddress[3]==0) {
                Config::store.ipAddress=IPAddress(10,0,0,20);
                Config::save();
            }
            Ethernet.begin(Config::store.mac,Config::store.ipAddress);
            Ethernet.setLocalIP(Config::store.ipAddress);
        }
        Display::ip();
        Display::status("network ready");
    }
    //-----------------------------------------------------------------------------------
    void restart() {
        if (!Config::store.dhcp) begin();
    }
    //-----------------------------------------------------------------------------------
    void  loop() {
        
    }
	//-----------------------------------------------------------------------------------
	void stop(){
	}    
    //-----------------------------------------------------------------------------------
}
#endif
