#ifndef __RGBLIGHT__
#define __RGBLIGHT__
#include <Arduino.h>
//=======================================================================================
#include "constants.h"
#include "mbedmon.h"
#include "display.h"
//=======================================================================================
namespace RGBLight {
    //-----------------------------------------------------------------------------------
    unsigned long stopMillis = 0;    
    //-----------------------------------------------------------------------------------
    void setColor(uint8_t r, uint8_t g, uint8_t b) {
        analogWrite(RGB_LIGHT_PIN_RED, r);
        analogWrite(RGB_LIGHT_PIN_GREEN, g);
        analogWrite(RGB_LIGHT_PIN_BLUE, b);
    }  
    //-----------------------------------------------------------------------------------
    void off(){
        setColor(0,0,0);
    }
    //-----------------------------------------------------------------------------------
    void stop(){
        off();
    }
    //-----------------------------------------------------------------------------------
    void blink(int8_t time, uint8_t r, uint8_t g, uint8_t b) {
        MBedMon::set("rgb","blink");
        setColor(r,g,b);
        stopMillis = millis() + time;
    }     
    //-----------------------------------------------------------------------------------
    void begin() {
        Display::status("rgb init");
        pinMode(RGB_LIGHT_PIN_RED, OUTPUT);
        pinMode(RGB_LIGHT_PIN_GREEN, OUTPUT);
        pinMode(RGB_LIGHT_PIN_BLUE, OUTPUT);  
        pinMode(RJ45_LIGHT_PIN, OUTPUT);  
        //off();
        Display::status("rgb ready");
    }
    //-----------------------------------------------------------------------------------
    void loop() {
        if (stopMillis > 0 && stopMillis < millis()) {
            off();
            stopMillis = 0;            
        }
    }
    //-----------------------------------------------------------------------------------
}
#endif
