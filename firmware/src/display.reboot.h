#ifndef __Display_Reboot__
#define __Display_Reboot__
//=======================================================================================
#include <U8g2lib.h>

#include "constants.h"
#include "modules.h"
#include "mbedmon.h"
#include "display.base.h"
//=======================================================================================
namespace Display_Reboot {
    //-----------------------------------------------------------------------------------
    void draw(U8G2_SSD1306_128X64_NONAME_F_HW_I2C&);
    void loop(U8G2_SSD1306_128X64_NONAME_F_HW_I2C&);
    //-----------------------------------------------------------------------------------
    void draw(U8G2_SSD1306_128X64_NONAME_F_HW_I2C &u8g2) {
        MBedMon::set("draw","reboot");
        u8g2.clearDisplay();
        u8g2.setFont(u8g2_font_10x20_tf);	
        u8g2.drawStr(34,36,"REBOOT");
        u8g2.sendBuffer();  
        delay(250);
        Modules::reboot();  
    }
    //-----------------------------------------------------------------------------------
    void loop(U8G2_SSD1306_128X64_NONAME_F_HW_I2C &u8g2) {
    }
    //-----------------------------------------------------------------------------------
    void btnEvent(Button btn, ButtonEvent evt){

    }
    //-----------------------------------------------------------------------------------
}
#endif
