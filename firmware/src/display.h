#ifndef __Display__
#define __Display__
//=======================================================================================
#include <SPI.h>
#include <Wire.h>
#include <U8g2lib.h>
#include <stdlib.h>
#include <Encoder.h>
#include <OneButton.h>

#include "constants.h"
#include "buildnumber.h"
#include "config.h"
#include "mbedmon.h"
#include "temperature.h"
//=======================================================================================
#include "display.base.h"
#include "display.off.h"
#include "display.main.h"
#include "display.dhcp.h"
#include "display.ip.h"
#include "display.leds.h"
#include "display.strips.h"
#include "display.universe.h"
#include "display.menu.h"
#include "display.reboot.h"
//=======================================================================================
namespace Display {
    U8G2_SSD1306_128X64_NONAME_F_HW_I2C u8g2(U8G2_R0, /* reset=*/ U8X8_PIN_NONE);
    Encoder uiEncoder(UI_ENCODER_CLOCKWISE_PIN, UI_ENCODER_ANTI_CLOCKWISE_PIN);
    //-----------------------------------------------------------------------------------
    Mode mode = DISPLAY_OFF;
    int32_t wheel_curr_pos=0, wheel_last_pos=0;
    unsigned long wheel_last_rotation=0;
    //-----------------------------------------------------------------------------------
    void begin();
    void loop();
    void stop();
    void refresh();
    void handleWheel();
    void status(const char*);
    void show(Mode mode);
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    void begin() {
        u8g2.begin();
        status("display start");
        show(DISPLAY_MAIN);
        Display_Base::exitCallback([](Mode mode){MBedMon::set("exitTo",static_cast<int>(mode)); show(mode);});
        Display_Base::btnDial.attachClick([](){
            switch (mode) {
                case DISPLAY_OFF: Display_Off::btnEvent(BUTTON_DIAL,BTN_CLICK); break;
                case DISPLAY_MENU: Display_Menu::btnEvent(BUTTON_DIAL,BTN_CLICK); break;
                case DISPLAY_MAIN: Display_Main::btnEvent(BUTTON_DIAL,BTN_CLICK); break;
                case DISPLAY_CONFIG_DHCP: Display_Dhcp::btnEvent(BUTTON_DIAL,BTN_CLICK); break;
                case DISPLAY_CONFIG_IP: Display_Ip::btnEvent(BUTTON_DIAL,BTN_CLICK); break;
                case DISPLAY_CONFIG_UNIVERSE: Display_Universe::btnEvent(BUTTON_DIAL,BTN_CLICK); break;
                case DISPLAY_CONFIG_STRIPS: Display_Strips::btnEvent(BUTTON_DIAL,BTN_CLICK); break;
                case DISPLAY_CONFIG_LEDS_PER_STRIP: Display_Leds::btnEvent(BUTTON_DIAL,BTN_CLICK); break;
                default: break;            
            }            
        });
        Display_Base::btnTest.attachClick([](){
            switch (mode) {
                case DISPLAY_OFF: Display_Off::btnEvent(BUTTON_TEST,BTN_CLICK); break;
                case DISPLAY_MENU: Display_Menu::btnEvent(BUTTON_TEST,BTN_CLICK); break;
                case DISPLAY_MAIN: Display_Main::btnEvent(BUTTON_TEST,BTN_CLICK); break;
                case DISPLAY_CONFIG_DHCP: Display_Dhcp::btnEvent(BUTTON_TEST,BTN_CLICK); break;
                case DISPLAY_CONFIG_IP: Display_Ip::btnEvent(BUTTON_TEST,BTN_CLICK); break;
                case DISPLAY_CONFIG_UNIVERSE: Display_Universe::btnEvent(BUTTON_TEST,BTN_CLICK); break;
                case DISPLAY_CONFIG_STRIPS: Display_Strips::btnEvent(BUTTON_TEST,BTN_CLICK); break;
                case DISPLAY_CONFIG_LEDS_PER_STRIP: Display_Leds::btnEvent(BUTTON_TEST,BTN_CLICK); break;
                default: break;            
            }            
        });
        Display_Base::btnDial.attachDuringLongPress([](){
            switch (mode) {
                case DISPLAY_OFF: Display_Off::btnEvent(BUTTON_DIAL,BTN_LONGPRESS_DURING); break;
                case DISPLAY_MENU: Display_Menu::btnEvent(BUTTON_DIAL,BTN_LONGPRESS_DURING); break;
                case DISPLAY_MAIN: Display_Main::btnEvent(BUTTON_DIAL,BTN_LONGPRESS_DURING); break;
                case DISPLAY_CONFIG_DHCP: Display_Dhcp::btnEvent(BUTTON_DIAL,BTN_LONGPRESS_DURING); break;
                case DISPLAY_CONFIG_IP: Display_Ip::btnEvent(BUTTON_DIAL,BTN_LONGPRESS_DURING); break;
                case DISPLAY_CONFIG_UNIVERSE: Display_Universe::btnEvent(BUTTON_DIAL,BTN_LONGPRESS_DURING); break;
                case DISPLAY_CONFIG_STRIPS: Display_Strips::btnEvent(BUTTON_DIAL,BTN_LONGPRESS_DURING); break;
                case DISPLAY_CONFIG_LEDS_PER_STRIP: Display_Leds::btnEvent(BUTTON_DIAL,BTN_LONGPRESS_DURING); break;
                default: break;            
            }            
        });
        Display_Base::btnTest.attachDuringLongPress([](){
            switch (mode) {
                case DISPLAY_OFF: Display_Off::btnEvent(BUTTON_TEST,BTN_LONGPRESS_DURING); break;
                case DISPLAY_MENU: Display_Menu::btnEvent(BUTTON_TEST,BTN_LONGPRESS_DURING); break;
                case DISPLAY_MAIN: Display_Main::btnEvent(BUTTON_TEST,BTN_LONGPRESS_DURING); break;
                case DISPLAY_CONFIG_DHCP: Display_Dhcp::btnEvent(BUTTON_TEST,BTN_LONGPRESS_DURING); break;
                case DISPLAY_CONFIG_IP: Display_Ip::btnEvent(BUTTON_TEST,BTN_LONGPRESS_DURING); break;
                case DISPLAY_CONFIG_UNIVERSE: Display_Universe::btnEvent(BUTTON_TEST,BTN_LONGPRESS_DURING); break;
                case DISPLAY_CONFIG_STRIPS: Display_Strips::btnEvent(BUTTON_TEST,BTN_LONGPRESS_DURING); break;
                case DISPLAY_CONFIG_LEDS_PER_STRIP: Display_Leds::btnEvent(BUTTON_TEST,BTN_LONGPRESS_DURING); break;
                default: break;            
            }            
        });
    }    
    //-----------------------------------------------------------------------------------
    void show(Mode __mode) {
        MBedMon::set("show",__mode);
        mode=__mode;
        switch (mode) {
            case DISPLAY_REBOOT: Display_Reboot::draw(u8g2); break;
            case DISPLAY_OFF: Display_Off::draw(u8g2); break;
            case DISPLAY_MENU: Display_Menu::draw(u8g2); break;
            case DISPLAY_MAIN: Display_Main::draw(u8g2); break;
            case DISPLAY_CONFIG_DHCP: Display_Dhcp::draw(u8g2); break;
            case DISPLAY_CONFIG_IP: Display_Ip::draw(u8g2); break;
            case DISPLAY_CONFIG_UNIVERSE: Display_Universe::draw(u8g2); break;
            case DISPLAY_CONFIG_STRIPS: Display_Strips::draw(u8g2); break;
            case DISPLAY_CONFIG_LEDS_PER_STRIP: Display_Leds::draw(u8g2); break;
            default: break;            
        }
    }    
    //-----------------------------------------------------------------------------------
    void loop() {
        Display_Base::btnDial.tick();
        Display_Base::btnTest.tick();
        handleWheel();
        switch (mode) {
            case DISPLAY_OFF: Display_Off::loop(u8g2); break;
            case DISPLAY_MENU: Display_Menu::loop(u8g2); break;
            case DISPLAY_MAIN: Display_Main::loop(u8g2); break;
            case DISPLAY_CONFIG_DHCP: Display_Dhcp::loop(u8g2); break;
            case DISPLAY_CONFIG_IP: Display_Ip::loop(u8g2); break;
            case DISPLAY_CONFIG_UNIVERSE: Display_Universe::loop(u8g2); break;
            case DISPLAY_CONFIG_STRIPS: Display_Strips::loop(u8g2); break;
            case DISPLAY_CONFIG_LEDS_PER_STRIP: Display_Leds::loop(u8g2); break;
            default: break;
        }        
    }    
    //-----------------------------------------------------------------------------------
    void stop(){
    }
    //-----------------------------------------------------------------------------------
    void refresh(){
        if (mode==DISPLAY_MAIN) show(DISPLAY_MAIN);
    }
    //-----------------------------------------------------------------------------------
    void status(const char *___status){
        strcpy (Display_Main::__status,___status);
        if (mode==DISPLAY_MAIN) Display_Main::status(u8g2);
    }
    //-----------------------------------------------------------------------------------
    void ip(){
        if (mode==DISPLAY_MAIN) Display_Main::ip(u8g2);
    }
    //-----------------------------------------------------------------------------------
    void handleWheel(){
        Display_Base::wheel = WHEEL_NONE;
        wheel_last_pos=wheel_curr_pos;
        wheel_curr_pos=uiEncoder.read();
        if (wheel_curr_pos>wheel_last_pos+WHEEL_FAST_TURN_DIFF) {
            Display_Base::wheel = WHEEL_RIGHT;
            if (wheel_curr_pos>wheel_last_pos+WHEEL_FAST_TURN_DIFF*2) Display_Base::wheel = WHEEL_FAST_RIGHT;
        } else if (wheel_curr_pos<wheel_last_pos-WHEEL_FAST_TURN_DIFF) {
            Display_Base::wheel = WHEEL_LEFT;
            if (wheel_curr_pos<wheel_last_pos-WHEEL_FAST_TURN_DIFF*2) Display_Base::wheel = WHEEL_FAST_LEFT;
        }
        if (millis() - wheel_last_rotation > WHEEL_DEBOUNCE_TIME && wheel_last_pos!=wheel_curr_pos) {
            wheel_last_rotation=millis();
            if (wheel_curr_pos<wheel_last_pos) Display_Base::wheel = WHEEL_LEFT;
            else if (wheel_curr_pos>wheel_last_pos) Display_Base::wheel = WHEEL_RIGHT;
        }        
    }
    //-----------------------------------------------------------------------------------

}
#endif
