#include <NativeEthernet.h>
#include <Wire.h>
#include <SPI.h>
#include <Arduino.h>

#include "constants.h"
#include "config.h"
#include "modules.h"
// ================================================================================
#include "mbedmon.h"
#include "display.h"
#include "display.base.h"
#include "network.h"
#include "artnetrcv.h"
#include "rgblight.h"
#include "temperature.h"
//#include "webapi.h"
// ================================================================================

void setup() {
    Wire.begin();
    Config::begin();
    MBedMon::baud(BAUD_RATE);
    // --- Modules ----------------------------------------------------------------
//    Modules::add(MBedMon::begin,      MBedMon::loop,     MBedMon::stop);
    Modules::add(Display::begin,      Display::loop,     Display::stop);
    Modules::add(Temperature::begin,  Temperature::loop, Temperature::stop);
    Modules::add(Network::begin,      Network::loop,     Network::stop);
    Modules::add(ArtnetRcv::begin,    ArtnetRcv::loop,   ArtnetRcv::stop);
//    Modules::add(WebApi::begin,       WebApi::loop,      WebApi::stop);
//    Modules::add(RGBLight::begin,     RGBLight::loop,    RGBLight::stop);
    // ----------------------------------------------------------------------------
    Display_Base::restartCallback(ArtnetRcv::restart);
    Display_Base::testCallback(ArtnetRcv::longTest);
    // ----------------------------------------------------------------------------
    Modules::begin();
    Display::status("system ready");
}
// ================================================================================
void loop() {
  Modules::loop();
}
// ================================================================================
