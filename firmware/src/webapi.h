#ifndef __WEBSERVER__
#define __WEBSERVER__
#include <NativeEthernet.h>
//=======================================================================================
#include "constants.h"
#include "config.h"
#include "mbedmon.h"
#include "buildnumber.h"
#include "artnetrcv.h"
#include "temperature.h"
#include "buildnumber.h"
#include "display.base.h"
//=======================================================================================
namespace WebApi {
    EthernetServer server(80);
    //-----------------------------------------------------------------------------------
    void htmlHeader(EthernetClient client) {
        client.println("HTTP/1.1 200 OK");
        client.println("Content-Type: text/html");
        client.println();
    }
    //-----------------------------------------------------------------------------------
    void begin() {
        server.begin();
    }    
    //-----------------------------------------------------------------------------------
    void stop() {
    }    
    //-----------------------------------------------------------------------------------
    void loop() {
        EthernetClient client = server.available();
        if (client) {
            while (client.connected()) {
                htmlHeader(client);
                client.print("{");
                client.print("\"build\":\"");
                client.print(BUILD_NUMBER);
                client.print("\",");

                client.print("\"uuid\":\"");
                client.print(Config::store.uuid);
                client.print("\",");

                client.print("\"dhcp\":");
                client.print(Config::store.dhcp ? "true":"false");
                client.print(",");

                client.print("\"version\":");
                client.print(Config::store.version);
                client.print(",");

                client.print("\"strips\":");
                client.print(Config::store.ledStrips);
                client.print(",");

                client.print("\"ledsPerStrip\":");
                client.print(Config::store.ledsPerStrip);
                client.print(",");

                client.print("\"startUniverse\":");
                client.print(Config::store.startUniverse);
                client.print(",");

                client.print("\"universeCount\":");
                client.print(Display_Base::universeCount);
                client.print(",");

                client.print("\"temperature\":");
                client.print(Temperature::read());
                client.print(",");

                char __mac[64];
                sprintf(__mac, "%02X:%02X:%02X:%02X:%02X:%02X", Config::store.mac[0],Config::store.mac[1],Config::store.mac[2],Config::store.mac[3],Config::store.mac[4],Config::store.mac[5]);
                client.print("\"mac\":\"");
                client.print(__mac);
                client.print("\"");

                client.print("}");
                client.stop();
            }
        }
    }
    //-----------------------------------------------------------------------------------
}
#endif
