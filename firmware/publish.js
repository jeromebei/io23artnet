const fs = require("fs");
const path = require("path");
const { exec } = require("child_process");
const nodemailer = require("nodemailer");

var options= {
    product: "io23artnet",
    user: "root",
    server: "builds.io23.io",
    port: 2323,
    remote: "/home/io23/builds/public_html/firmware/io23artnet/",
    link:   "https://builds.io23.io/firmware/io23artnet/",
    local: path.join(__dirname,"/builds"),
    mail: require("./mailsettings.json"),
    recipients: "jbei@icloud.com,max.schmit@gmail.com"
}

console.log("publishing io23artnet firmware");
fs.readdir(options.local, (err, files)=>{
    files.forEach(function (file) {
        var consider = file.endsWith(".hex");
        if (consider) {
            var build = path.basename(file,".hex");
            build = build.split(".");
            if (!options.build || options.build[2] < build[2]) options.build = build.join(".");
        }
    });
    options.file = options.build+".hex";
    options.changelog = options.link+"CHANGELOG.md";
    options.link = options.link+options.file;
    console.log(`uploading build ${options.build} to ${options.link}`);
    exec(`scp -P ${options.port} ${path.join(options.local,options.file)} ${options.user}@${options.server}:${path.join(options.remote,options.file)}`, (error, stdout, stderr) => {
        if (error)  return console.log(`error: ${error.message}`);
        //if (stderr) return console.log(`stderr: ${stderr}`);
        console.log(`uploading build changelog to ${options.changelog}`);
            exec(`scp -P ${options.port} ${__dirname}/CHANGELOG.md ${options.user}@${options.server}:${path.join(options.remote,"CHANGELOG.md")}`, (error, stdout, stderr) => {
            if (error)  return console.log(`error: ${error.message}`);
            //if (stderr) return console.log(`stderr: ${stderr}`);
            notify().then(()=>{
                console.log(`done`);
            });
        });    
    });    

});

async function notify() {
    console.log(`sending email(s)`);
    let transporter = nodemailer.createTransport({
        host: options.mail.host,
        port: options.mail.port,
        secure: false, 
        auth: {
          user: options.mail.user, 
          pass: options.mail.pass, 
        },
      });
      let info = await transporter.sendMail({
        from: '"Build Mailer" <jbei@icloud.com>', 
        to: options.recipients, 
        subject: options.product + " build "+options.build+" ready for download", 
        text: `Build download: ${options.link}\nChangelog: ${options.changelog}`, 
        html: null, 
      });
}